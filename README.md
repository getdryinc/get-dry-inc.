Get Dry, Inc. is a water damage mitigation and mold remediation company based in Boynton Beach and serving all of Palm Beach County.

Address: 4736 Lake Worth Rd, Greenacres, FL 33463, USA

Phone: 561-777-2618

Website: http://www.getdryinc.com
